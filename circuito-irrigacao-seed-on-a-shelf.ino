// Pinos - Botoes
int increasePin = 4; //gpio4(D2)
int decreasePin = 16; //gpio16(D0)
int confirmPin = 5; //gpio5(D1)
//int relayPin1 = 14; //gpio14(D5)
int relayPin1 = 7;  // arduino
int relayPin2 = 6;  // arduino

// Pinos - led
int confirmLed = 2; //gpio2(D4)
int increase = 0;
int decrease = 0;
int confirm = 0;
long int ledTime = 60;

// contador 
int i = 0;
int timer = 10;
int waterCounter = 0;
int lightCounter = 0;
long long maxSeconds = 86400;

// Horários de ativacão (em segundos do dia)
//int lightMoments[] = {0};   // DEBUG
//int waterMoments[] = {0, 60, 120, 180}; //DEBUG

int lightMoments[] = {10};  // 10 segundos de setup
int waterMoments[] = {21600,43200,64800,86400};
//int waterMoments[] = {60, 120};

// Flags de ativacão
bool waterOn = false;
bool lightOn = false;

// Tempos de operacão (CONFIGURACÃO)
long long waterTimer = 10;
long long lightTimer = 64800;

/* 
  Funcão que itera no array de tempo do dispositivo
  e verifica se o valor atual do timer é um dos horários de ativacão
*/
bool isTurnCircuitTime(int counter, int momentArray[], int arraySize) {
  bool activate = false;
  
  for (int i = 0; i < arraySize; i++) {
    if (counter == momentArray[i]) {
      activate = true;
    }
  }
  return activate;
}

/* Funcão que printa o output pra serial formatado (temporário)*/
void printFormatted(int counter1, int counter2, int counter3) {
  Serial.print("counter=");
  Serial.print(counter1);
  Serial.print(", water_counter=");
  Serial.print(counter2);
  Serial.print(", light_counter=");
  Serial.print(counter3);
  Serial.print(", light_on=");
  Serial.print(lightOn);
  Serial.print(", water_on=");
  Serial.print(waterOn);
  Serial.print("\n");
}

void setup() {
  // put your setup code here, to run once:
//  Serial.begin(115200);
   Serial.begin(9600);  // arduino uno
  
  // Botões
  pinMode(increasePin, INPUT); // botao aumentar
  pinMode(decreasePin, INPUT); // botao diminuir
  pinMode(confirmPin, INPUT); // botao confirmar

  // Leds
  pinMode(confirmLed, OUTPUT);

  // Relés
  pinMode(relayPin1, OUTPUT);
  pinMode(relayPin2, OUTPUT);
  
  digitalWrite(relayPin1, HIGH);
  digitalWrite(relayPin2, HIGH);
  
 
 }

void loop() {

     // Verifica se i é igual a algum horário de lugar luz
    if (isTurnCircuitTime(i, waterMoments, sizeof(waterMoments)/sizeof(int))) {
      waterOn = true;
      digitalWrite(relayPin2, LOW);
    }

    // Verifica se i é igual a algum horário de lugar luz
    if (isTurnCircuitTime(i, lightMoments, sizeof(lightMoments)/sizeof(int))) {
      lightOn = true;
      digitalWrite(relayPin1,LOW);
    }


    // Verifica se os dispositivos estão ligados e sobem o timer
    if (lightOn) {
      lightCounter++;
    }
    if (lightCounter > lightTimer) {
        lightCounter = 0;
        lightOn = false;
        digitalWrite(relayPin1,HIGH);
      }

    
    if (waterOn) {
      waterCounter++;
    }
    if (waterCounter > waterTimer) {
        waterCounter = 0;
        waterOn = false;
        digitalWrite(relayPin2,HIGH);
     }
    
    // Zera o contador se chegou ao máximo do dia
    if (i >= maxSeconds) {
      i = 0;
    }

  // Sobe o contador
  i++;
  printFormatted(i,waterCounter, lightCounter);
  delay(1000);

}
