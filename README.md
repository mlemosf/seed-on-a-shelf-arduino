# Seed on a Shelf - Programas Arduino

Repositório para manter as versões do programa principal dos canteiros autônomos Seed on a Shelf.  
**Esse repositório pode ser movido sem aviso prévio por motivos de mudanca de nome da empresa**.

## Execucão

Para carregar o projeto é necessário ter uma placa **Arduino/Genuino Uno**. O programa
não foi testado em placas alternativas.  
Basta carregar o programa **circuito-irrigacao-seed-on-a-shelf.ino**.

## Configuracão

O programa permite a definicão de novos valores para as variáveis de timer, de modo
a manter luzes e irrigacão ligadas por mais ou menos tempo, dependendo da demanda
das plantas sendo cultivadas. As variáveis recebem o valor de tempo em segundos. Por
padrão, a luz fica ligada por 

	// Tempos de operacão (CONFIGURACÃO)
	int waterTimer = 10;
	int lightTimer = 64800;


